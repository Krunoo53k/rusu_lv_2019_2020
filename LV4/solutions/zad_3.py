import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

def draw_line(x, theta):
    return theta[1]*x+theta[0]

def differential(x,y,theta):
    b = 0.0
    k = 0.0
    for i in range(0,x.shape[0]):
          b+=draw_line(x[i],theta)-y[i]
          k+=(draw_line(x[i],theta)-y[i])*x[i]
    b /= x.shape[0]
    k /= x.shape[0]
    return b,k

def criteria_function(x,y,theta):
    n=x.shape[0]
    J=0.0
    for i in range (0,n):
        J+=pow((draw_line(x[i],theta)-y[i]),2)
    return J/(2*n)

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)

x_vektor=np.ones((len(x),1))
x_vektor=np.append(x_vektor,x,axis=1)
theta = np.linalg.inv(np.transpose(x_vektor) @ x_vektor) @ np.transpose(x_vektor) @ y_measured
ypravrav=np.array([draw_line(x.min(),theta), draw_line(x.max(),theta)])
plt.plot([x.min(),x.max()],ypravrav)


yprav=np.zeros((2,1))
iter=400
theta=np.array([0,0])
learning_rate=0.00004
theta = np.zeros((2,1))
J=np.zeros((iter,1))
for i in range(0,iter):
    J[i]=criteria_function(x,y_measured,theta)
    b,k=differential(x,y_measured,theta)
    theta[0] = theta[0] - learning_rate * b
    theta[1] = theta[1] - learning_rate * k
yprav[0] = draw_line(x.min(),theta)
yprav[1] = draw_line(x.max(),theta)
plt.plot([x.min(),x.max()],yprav,'cyan')


plt.show()