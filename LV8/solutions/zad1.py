import csv
import os


imageInfo=[]
with open('./Test.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in reader:
        imageInfo.append([i.split('\n', 1)[0] for i in row])

imageClassIndex = imageInfo[0].index('ClassId')
imagePathIndex = imageInfo[0].index('Path')

for i in range(0,43):
    if not os.path.exists(f"./Test/{i}"):
        os.makedirs(f"./Test/{i}")

for i in range(1,len(imageInfo)):
    load_path = f"./{imageInfo[i][imagePathIndex]}"
    image_name = imageInfo[i][imagePathIndex][5:]
    image_class = imageInfo[i][imageClassIndex]
    save_path = f"./Test/{image_class}/{image_name}"
    os.replace(load_path,save_path)
