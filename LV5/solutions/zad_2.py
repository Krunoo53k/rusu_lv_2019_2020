from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn import cluster


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[[1.0, 2.5], [0.5, 3.0], [1.5, 2.5]],
                          random_state=random_state)


    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

flagc=5
J=[]

data=generate_data(500, flagc)
optimal_center_amount=0
for hihi in range(1,20):
    kmeans=cluster.KMeans(n_clusters=hihi, random_state=5).fit(data)
    y_kmeans = kmeans.predict(data)
    if(len(J)>0):
        if(kmeans.inertia_<min(J)*0.8):
            optimal_center_amount=hihi
    J.append(kmeans.inertia_)

kmeans=cluster.KMeans(n_clusters=optimal_center_amount, random_state=5).fit(data)
plt.scatter(data[:, 0], data[:, 1], c=y_kmeans, s=50)

centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='cyan');

plt.figure()
plt.plot(np.arange(19)+1,J)
plt.xlabel("Num of centroids")
plt.ylabel("Errors")
plt.legend()
plt.show()

print("Optimal amount of centers is ",optimal_center_amount)
