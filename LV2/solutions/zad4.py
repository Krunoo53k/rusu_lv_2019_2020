import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt

#Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). Pomoću histograma prikažite rezultat ovih bacanja. 

def graf(podaci,boja):
    plt.hist(podaci, color=boja, density=True, edgecolor='k',bins=np.arange(2, 14)/2)
    plt.axvline(podaci.mean(), color='k', linestyle='dashed', linewidth=1.5)
    min_ylim, max_ylim = plt.ylim()
    plt.text(podaci.mean()*1.005, max_ylim*0.9, 'Mean: {:.2f}'.format(podaci.mean()))
    
rng=default_rng()

bacanja=rng.integers(low=1, high=7, size=100)

graf(bacanja, "r")