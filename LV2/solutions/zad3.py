# Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne vrijednosti 0 ili 1. Neka 1
# označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem
# odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm i
# standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija sa srednjom vrijednošću 167 cm
# i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) odnosno crvenom bojom (0). Napišite
# funkciju koja računa srednju vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću funkcije
# np.dot). Prikažite i dobivene srednje vrijednosti na grafu.

import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt

rng=default_rng()

spolovi=rng.integers(2, size=(1,100))
muski_visina=np.random.normal(180,7,size=np.count_nonzero(spolovi==1))
zenske_visina=np.random.normal(167,7,size=np.count_nonzero(spolovi==0))

def graf(podaci, boja):
    plt.hist(podaci, color=boja, density=True, edgecolor='k')
    plt.axvline(podaci.mean(), color='k', linestyle='dashed', linewidth=1)
    min_ylim, max_ylim = plt.ylim()
    plt.text(podaci.mean()*1.005, max_ylim*0.9, 'Mean: {:.2f}'.format(podaci.mean()))
    
plt.subplot(1,2,1)
plt.title("Male height distribution")
graf(muski_visina,'g')


plt.subplot(1,2,2)
plt.title("Female height distribution")
graf(zenske_visina,'r')

plt.show()