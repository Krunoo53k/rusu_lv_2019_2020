import numpy as np
import matplotlib.pyplot as plt
import csv

car_info=[]

with open("mtcars.csv", newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in spamreader:
        car_info.append([item.split('\n', 1)[0] for item in row])

car_base=[]
car_base.append(car_info[0])
car_info.pop(0)

plt.figure(1)

for car in car_info:
    plt.plot([0, float(car[1])],[0,float(car[4])], label="car[6]")

plt.show()

potrosnja=[]

for auto in car_info:
    potrosnja.append(float(auto[1]))

potrosnja=np.array(potrosnja)

print("Minimialna vrijednost je: "+ str(np.amin(potrosnja)))
print("Maksimalna vrijednost je: "+ str(np.amax(potrosnja)))
print("Srednja vrijednost je: "+ str(np.average(potrosnja)))
