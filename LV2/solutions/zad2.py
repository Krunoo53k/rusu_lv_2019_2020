from os import replace
import re

#Napišite Python skriptu koja će učitati tekstualnu datoteku, pronaći valjane email adrese te izdvojiti samo prvi dio
#adrese (dio ispred znaka @). Koristite odgovarajući regularni izraz. Koristite datoteku mbox-short.txt. Ispišite rezultat.

file = open("mbox-short.txt", "r")
useri=[]

for line in file:
    lst = re.findall('[a-zA-Z0-9]*@\S+', line)
    for email in lst:
        usernameovi=(re.findall('\S*@', email))
        useri.append(usernameovi[0])

useri=' '.join(useri).replace('@','').split()

#Pretvorit cu u jedan veliki string da bi findall bio sretan
user_names=""
for user in useri:
    user_names+=user+" "

print(user_names)
#1. sadrži najmanje jedno slovo a 
print("Sadrzi najmanje jedno slovo a:")
print(re.findall('[^ ]\S*a*\S*',user_names))
#2. sadrži točno jedno slovo a 
print("Sadrzi samo jedno slovo a:")
print(re.findall(' [^ a]*a[^ a]* ',user_names))
#3. ne sadrži slovo a 
print("Ne sadrzi slovo a:")
print(re.findall(' [^ a]* ',user_names))
#4. sadrži jedan ili više numeričkih znakova (0 – 9) 
print("Sadrži jedan ili više numeričkih znakova:")
print(re.findall(' \S*[0-9][0-9]*\S* ',user_names))
#5. sadrži samo mala slova (a-z)
print("Sadrži sadrži samo mala slova (a-z):")
print(re.findall(' [a-z]* ',user_names))