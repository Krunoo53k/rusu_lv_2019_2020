import numpy as np

import matplotlib.pyplot as plt 

import matplotlib.image as mpimg 

import numpy as np 

 

img = mpimg.imread('tiger.png') 
plt.figure(1)
plt.imshow(img)

img *= 2
img = np.clip(img, 0, 1)
plt.figure(2)
plt.imshow(img)

plt.show()