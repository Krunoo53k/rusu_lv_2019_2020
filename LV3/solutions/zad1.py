import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

#1
mtcars=mtcars.sort_values(by=["mpg"], ascending=True)
print(mtcars[0:5])

#2
mtcars=mtcars.sort_values(by=["mpg"], ascending=False)
print(mtcars.query('cyl == [8]')[0:5]) 

#3
print("Potrosnja 6 cilindara: ",mtcars[mtcars.cyl == 6].mpg.mean()) 

#4
print("Potrosnja 4 cilindara i ono tezina: ", mtcars[(mtcars.cyl == 4) & (2.000<=mtcars.wt) & (mtcars.wt<=2.200)].mpg.mean())

#5
print("Auta bez automatskog: ", len(mtcars[mtcars.am==0]))
print("Auta s automatskog", len(mtcars[mtcars.am==1]))

#6
print("Auta s automatskim i konje preko 100: ", len(mtcars[(mtcars.am==1)&(mtcars.hp>100)]))

#7
print(mtcars['wt']*0.4536)