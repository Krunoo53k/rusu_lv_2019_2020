import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars=pd.read_csv("../resources/mtcars.csv")

#1
mtcars[['mpg','cyl']].groupby('cyl').mean().plot.bar(rot=0, color='beige')

#2
mtcars[['wt','cyl']].boxplot(by='cyl')

#3 i 4
mtcars[['mpg','qsec','hp','am']].groupby('am').mean().plot.bar(rot=0)
