import pandas as pd
import xml.etree.ElementTree as ET
import numpy as np
import urllib.request

# url that contains valid xml file:
# url = 'http://iszz.azo.hr/...'


url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot()

# add date month and day designator

df['month'] = pd.to_datetime(df['vrijeme'], utc=True).dt.month
df['dayOfweek'] = pd.to_datetime(df['vrijeme'], utc=True).dt.dayofweek
df['year'] = pd.to_datetime(df['vrijeme'], utc=True).dt.year

# ZAD 1
mjerenja=df[df['year']==2017]['mjerenje']

# ZAD 2
mjerenja=df[df['year']==2017][['mjerenje','vrijeme']].sort_values(by='mjerenje')[-3:]
print(mjerenja['vrijeme'])

# ZAD 3
mjeseci=['Siječanj','Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac']
mjeseci_brod_dana=np.array([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])

zapisi=np.array(df[['mjerenje','month']].groupby('month').count()).reshape(12,)
missing_zapisi = (mjeseci_brod_dana - zapisi).tolist()

neispravni_rezultati = pd.DataFrame({'Missing zapisi':missing_zapisi}, index= mjeseci)
neispravni_rezultati.plot.bar()

# ZAD 4
raz = df[(df['month'] == 1) | (df['month']== 8)][['month','mjerenje']]
raz.boxplot(column = ['mjerenje'], by = 'month')

# ZAD 5
df['weekend'] = (df['dayOfweek'] == 0) | (df['dayOfweek'] == 6)
df.boxplot(column = ['mjerenje'], by = 'weekend')


