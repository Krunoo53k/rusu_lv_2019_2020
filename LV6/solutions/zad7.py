import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import PolynomialFeatures

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def probabilityClass(h0):
    return h0 >= 0.5

np.random.seed(242)
train_data=generate_data(200)

np.random.seed(12)
test_data=generate_data(100)


poly = PolynomialFeatures(degree=3, include_bias = False)
data_train_new = poly.fit_transform(train_data[:,0:2])
reg=LogisticRegression().fit(data_train_new,train_data[:,2])

plt.scatter(test_data[:,0], test_data[:,1], c=probabilityClass(reg.predict(poly.fit_transform(test_data[:,0:2]))))
plt.show()
