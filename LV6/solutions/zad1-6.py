import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix


def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def probabilityClass(h0):
    return h0 >= 0.5

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

np.random.seed(242)
learn_dat=generate_data(200)
np.random.seed(12)
test_dat=generate_data(100)

plt.scatter(learn_dat[:,0], learn_dat[:,1], c=learn_dat[:,2], cmap='cividis')

reg=LogisticRegression().fit(learn_dat[:,0:2],learn_dat[:,2])

xp = np.array([learn_dat[:,0].min(), learn_dat[:,0].max()])
yp1 = -reg.coef_[0][0]/reg.coef_[0][1] * xp[0] - reg.intercept_[0]/reg.coef_[0][1]
yp2 = -reg.coef_[0][0]/reg.coef_[0][1] * xp[1] - reg.intercept_[0]/reg.coef_[0][1]
yp = np.array([yp1,yp2])

plt.scatter(learn_dat[:,0], learn_dat[:,1], c=probabilityClass(reg.predict(learn_dat[:,0:2])))
plt.plot(xp,yp,'r')
plt.show()

#4. zad
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(learn_dat[:,0])-0.5:max(learn_dat[:,0])+0.5:.05,
                          min(learn_dat[:,1])-0.5:max(learn_dat[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = reg.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)

ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()


#5. zad


logreg=LogisticRegression().fit(learn_dat[:,0:2],learn_dat[:,2])

predictedClass=probabilityClass(logreg.predict(test_dat[:,0:2]))

correctPredictions=test_dat[predictedClass==test_dat[:,2]][:,0:2]
incorrectPredictions=test_dat[predictedClass!=test_dat[:,2]][:,0:2]

plt.scatter(correctPredictions[:,0], correctPredictions[:,1], c='g')
plt.scatter(incorrectPredictions[:,0], incorrectPredictions[:,1], c='k')
plt.show()

c_matrix=confusion_matrix(test_dat[:,2], predictedClass)
plot_confusion_matrix(c_matrix)

