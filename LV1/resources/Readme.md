Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. 
Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value).
Međutim, skripta ima bugove i ne radi kako je zamišljeno.
----------------------------------------
Prvi bug je korištenje fnamex umjesto fname.
Nakon toga, koristi se input, koji je ostatak starije python verzije te nije kompatibilan s 3.7 verzijom.
Također, "exit()" nije definiran te trebamo koristiti sys biblioteku.
Funkcija neispravno broji ponavljanje riječi, točnije stavlja da se svaka riječ ponavlja samo jednom, pa je potrebno postaviti brojač.