import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))
# TODO: prikazi nekoliko slika iz train skupa
plt.subplots(12)
c=1
for slika in x_train[0:11]:
  plt.subplot(3,4,c)
  plt.imshow(slika, cmap='gray')
  c+=1
# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras.Sequential(
    [
        keras.Input(shape=input_shape),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Flatten(),
        layers.Dropout(0.5),
        layers.Dense(num_classes, activation="softmax"),])
model.summary()
# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
batch_size = 128
epochs = 15
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
model.fit(x_train_s, y_train_s, batch_size=batch_size, epochs=epochs, validation_split=0.1)

# TODO: provedi ucenje mreze
x_val=x_train_s[-10000:]
y_val=y_train_s[-10000:]
x_train_s=x_train_s[:-10000]
y_train_s=y_train_s[:-10000]
history=model.fit(
    x_train_s,
    y_train_s,
    batch_size=64,
    epochs=2,
    validation_data=(x_val,y_val))
# TODO: Prikazi test accuracy i matricu zabune
y_predicted = model.predict(
    x_test_s, batch_size=128, verbose=0, steps=None, callbacks=None, max_queue_size=10,
    workers=1, use_multiprocessing=False
)

# classify predicted values
y_predicted[y_predicted>=0.5] = 1
y_predicted[y_predicted<0.5] = 0

from sklearn.metrics import confusion_matrix
confusion_matrix(y_test_s.argmax(axis=1), y_predicted.argmax(axis=1))

score = model.evaluate(x_test_s, y_test_s, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])


# TODO: spremi model

filename = 'sample_data/model'
model.save(filename)